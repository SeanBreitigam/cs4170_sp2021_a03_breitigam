# Hyrbid Parallelization (OpenMP+MPI) PSO #

Completed by **Sean Breitigam**

## Summary ##
My work provides a hybrid parallelization of Particle Swarm Optimization (PSO) utilizing the Rastrigin function for fitness evaluation. Hybrid parallelization was achieved through the use of MPI for process communication and OpenMP for thread parallelization. This program was run on the Ohio Supercomputing Center to provide consistent and fast results. Overall my program saw huge speed improvements, close to linear speedups. At 10 threads, 10 processes, and Np of 1000 there was a speedup of 75.4. My efficiency did have some low parts, down to 69% at times, but overall it mostly stayed above 75%. Further improvements may be possible through the implementation of nested parallelism within OpenMP and minimized amounts of overhead.

## Instructions ##
To compile and run locally:
```
docker run --rm -v %cd%:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v %cd%:/tmp -w /tmp/Default rgreen13/alpine-bash-gpp mpiexec --allow-run-as-root -n X MPI
```

To compile on OSC:
```
make clean
make OSC
```

To run on OSC:
```
sbatch jobScript.slurm
```

## Algorithm ##
Particle Swarm sets particles, Np, in a dimensional space, Nd. These particles then move at certain velocities (also randomly generated) to achieve better fitness. These position's fitness is evaluated using the [Rastrigin function](https://www.sfu.ca/~ssurjano/rastr.html). Many fitness functions can be used to evaluate PSO, they essentially provide feedback on how close results are to the desired results. The optimal fitness is 0, with repeated evaluations on the particles, they drift toward optimal fitness.

## Method ##
I started this assignment with a base code of John Bartocci's assignment 2 that parallelized PSO using MPI. The MPI assigns a subset swarm to each process based on the total number of processes. The OpenMP implementations then further divide the problems within the processes. Most of the OpenMP is used on the calculations within the actual PSO operations while MPI communicates the best positions and values between nodes to provide for a more accurate next step.
## Discussion and Analysis ##
All results are available in [CSV](./Results/results.csv) and [Excel]("./Results/ResultsA03.xlsx") format. Performance was evaluated using 5 trials for combinations of threads ranging from 1-10, processes ranging from 1-10, and a particle population size of 1000. Tables are available in the data files with charts shown below in 3D formats for visual insight. 

The results show that speedup is extremely close to linear, both dipping slightly below & above at times. The speedup consistently increases when either the threads or processes are increased, especially when both are increased. The speedup is analyzed at a high problem size (Np = 1000), so it may not see as big of speedups at the lower iterations.

Efficiency decreases slightly as the number of resources increase. The rate of efficiency decrease does not seem to vary much when comparing a number of threads and processors, both decreasing at approximately the same rates. Efficiency falls to around 75%, which is not bad, but there is a slight decrease in resource utilization. Note this is also at an Np of 1000 which does not account for low iterations where efficiency is usually seen to decrease because of the increased overhead and less utilization for a small problem size.

The Karp-Flatt metric is a bit messed up. This is because Karp-Flatt only accounts for a single resource increasing and since I increased threads and processes, the formula is inaccurate. Although looking at the Karp-Flatt results, it is still a relatively flat and low number which indicates a low amount of serial code and also suggests consistency in performance when the resources and problem sizes are scaled.

<div style="align:center;">

![3D Speedup](./Results/Speedup.png "3D Speedup")

![3D Efficiency](./Results/Efficiency.png "3D Efficiency")

![3D Karp-Flatt](./Results/KarpFlatt.png "3D Karp-Flatt")
</div>


