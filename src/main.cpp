/***
 * 
 * Sourced from John Bartocci's assignment 2 code
 * 
 * 
 * Edited + updated for assignment 3 
 * Sean Breitigam
 * 
 ***/


#include <iostream>
#include <iomanip>
#include <math.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <random>

#include <omp.h>
#include "CStopWatch.h"

#include <mpi.h>
#include <boost/mpi.hpp>
#include <boost/mpi/environment.hpp>
#include <boost/mpi/communicator.hpp>
#include <boost/mpi/collectives.hpp>
#include <boost/serialization/vector.hpp>

namespace mpi = boost::mpi;


#define PI_F 3.141592654f

int threads;

std::random_device rd;                          // only used once to initialise (seed) engine
double randDbl(const double& min, const double& max) {
    static thread_local std::mt19937* generator = nullptr;
    if (!generator) {
        generator = new std::mt19937(clock());
    }
    std::uniform_real_distribution<double> distribution(min, max);
    return distribution(*generator);
}

double Rastrigin(std::vector<double> &R, int Nd){ 
    double Z = 0, Xi;

    for (int i = 0; i < Nd; i++){
        Xi = R[i];
        Z += (pow(Xi, 2) - 10 * cos(2 * PI_F * Xi) + 10);
    }
    return -Z;
}

class Particle {
	public:
		Particle(int N, float xMin, float xMax, float vMin, float vMax, double (*objFunc)(std::vector<double> &, int));
		Particle();
		
					
		std::vector<double> x;   		
		std::vector<double> v;       	
		std::vector<double> x_best;  	
		double p_best = 0;		
		int N;
		double (*rPtr)(std::vector<double> &, int) = NULL;
		float xMin;
		float xMax;
		float vMin;
		float vMax;
		
				
		double fitness();
		
		void updatePosition();
		void updateVelocity(float w, float C1, float C2, double * g_best);
	
	private:
		friend class boost::serialization::access;

		template<class Archive>
		void serialize(Archive & ar, const unsigned int version)
		{
			ar & x;
			ar & v;
			ar & x_best;
			ar & p_best;
		}
	
	
};


Particle::Particle(int N_, float xMin_, float xMax_, float vMin_, float vMax_, double (*objFunc)(std::vector<double> &, int)) {
	N = N_;
	xMin = xMin_;
	xMax = xMax_;
	vMin = vMin_;
	vMax = vMax_;
	x.resize(N, 0);
	v.resize(N, 0);
	x_best.resize(N, 0);
	for(int i=0; i < N; i++){
		x[i] = randDbl(xMin, xMax);
		v[i] = randDbl(vMin, vMax);
		x_best[i] = x[i];
	rPtr = objFunc;
	p_best = rPtr(x,N);	
	}	
}

Particle::Particle(void){}

double Particle::fitness(){
	double currentFit;
	currentFit = rPtr(x, N);
	if(currentFit > p_best){
		p_best = currentFit;
		for(int i=0; i < N; i++){
			x_best[i] = x[i];
		}
	}
	return currentFit;
}


void Particle::updatePosition(){
	for (int i = 0; i < N; i++){
		x[i] = x[i] + v[i];

		if (x[i] > xMax){ x[i] = randDbl(xMin, xMax);}
		if (x[i] < xMin){ x[i] = randDbl(xMin, xMax);}
	}
}


void Particle::updateVelocity(float w, float C1, float C2, double * g_best){
	for (int i = 0; i < N; i++){
		float R1 = randDbl(0, 1);
		float R2 = randDbl(0, 1);

		// Original PSO
		v[i] = w * v[i] + C1 * R1 * (x_best[i] - x[i]) + C2 * R2 * (g_best[i] - x[i]);
		if (v[i] > vMax){ v[i] = randDbl(vMin, vMax);}  
		if (v[i] < vMin){ v[i] = randDbl(vMin, vMax);}
	}
}


void PSO(const int Np, const int Nd, int Nt, float xMin, float xMax, float vMin, float vMax,
         double (*objFunc)(std::vector<double> &, int), std::string functionName)
{
	CStopWatch totalTimer, commTimer, myTimer, velTimer;
	float positionTime = 0, fitnessTime = 0, velocityTime = 0, totalTime = 0, commTime = 0;
	totalTimer.startTimer();	
	mpi::communicator world;
	int myRank = world.rank();
	int localNp = Np/world.size();
	int tempIndex = 0;
	double tempFitness, gBestValueOld;
	double bestBuf[world.size()];
	double bestParticleBuf[Nd*world.size()]; 
	if(myRank < Np%world.size()){
		localNp += 1;
	}
    std::vector<Particle *> localSwarm; 
	double gBestValue = -INFINITY;
	double gBestPosition [Nd];
	tempFitness = -INFINITY;
	float C1 = 1.5, C2 = 1.5;
    float w, wMax = 0.9, wMin = 0.1;

	//omp_set_dynamic(0);
	omp_set_num_threads(threads);
	
	
	
	// Initialize the local swarms
	//#pragma omp parallel private(tempFitness, tempIndex)
    //{
        //#pragma omp for schedule(auto)
		for(int i=0; i < localNp; i++){
			localSwarm.push_back(new Particle(Nd, xMin, xMax, vMin, vMax, objFunc));
			if(localSwarm[i]->p_best > tempFitness) {
				tempFitness = localSwarm[i]->p_best;
				tempIndex = i;
			}
		}
	//}
		// Initialize the global best of the local swarms
		gBestValue = tempFitness;
		//#pragma omp for //schedule(auto)
		for(int z=0; z < Nd; z++){
			gBestPosition[z] = localSwarm[tempIndex]->x[z];
		}
	
	
	commTimer.startTimer();
	mpi::gather(world, gBestValue, &bestBuf[0], 0);
	mpi::gather(world, &gBestPosition[0], Nd, &bestParticleBuf[0], 0);
	commTimer.stopTimer();
	commTime += commTimer.getElapsedTime();
	
	if(myRank == 0){
		tempIndex = -1;
		gBestValue = -INFINITY;
			for(int i=0; i < world.size(); i++){
				if(bestBuf[i] > gBestValue){
					gBestValue = bestBuf[i];
					tempIndex = i;
				}
			}
			if(tempIndex != -1){
				#pragma omp parallel
				{
				#pragma omp for schedule(auto)
				for(int z=0; z < Nd; z++){
					gBestPosition[z] = bestParticleBuf[tempIndex*Nd+z];
				}
				}
		}
	}
	commTimer.startTimer();
	mpi::broadcast(world, gBestValue, 0);
	mpi::broadcast(world, &gBestPosition[0], Nd, 0); 
	commTimer.stopTimer();
	commTime += commTimer.getElapsedTime();
	
	//For a number of iterations Nt...
	for (int j = 1; j < Nt; j++){
		myTimer.startTimer();
		
		//update position
		#pragma omp parallel 
		{
			//std::cout << omp_get_num_threads() << "NUM THREADS" << std::endl;
			#pragma omp for schedule(auto)
			for(int i = 0; i < localNp; i++){
				localSwarm[i]->updatePosition();
			}
		
				
		myTimer.stopTimer();
		positionTime += myTimer.getElapsedTime();
		myTimer.startTimer();
		
		//update fitness
		tempIndex = -1;
		//#pragma omp parallel 
		//{
			#pragma omp for schedule(auto)
			for(int i = 0; i < localNp; i++){
				tempFitness = localSwarm[i]->fitness();
				if(tempFitness > gBestValue){
					tempIndex = i;
				}
			}
		//}
		
		gBestValueOld = gBestValue;
		if(tempIndex != -1){
			//#pragma omp parallel 
			//{
				#pragma omp for schedule(auto)
				for(int i = 0; i < Nd; i++){
					gBestPosition[i] = localSwarm[tempIndex]->x[i];
				}
			//}
			gBestValue = tempFitness;
		}
		}
		
		
		//------------------reconcile global bests
		commTimer.startTimer();
		mpi::gather(world, gBestValue, &bestBuf[0], 0);			
		//
		commTimer.stopTimer();
		commTime += commTimer.getElapsedTime();
		
		tempIndex = -1;
		if(myRank == 0){
			for(int i=0; i < world.size(); i++){
				if(bestBuf[i] > gBestValue){
					gBestValue = bestBuf[i];
					tempIndex = i;					
				}
			}
			
		}
		
		
		commTimer.startTimer();
		mpi::broadcast(world, gBestValue, 0);
		commTimer.stopTimer();
		commTime += commTimer.getElapsedTime();
		
		if(gBestValue != gBestValueOld){
			commTimer.startTimer();
			mpi::gather(world, &gBestPosition[0], Nd, &bestParticleBuf[0], 0);
			commTimer.stopTimer();
			commTime += commTimer.getElapsedTime();
			if(myRank == 0){
				if(tempIndex != -1){
					#pragma omp parallel
					{
						#pragma omp for schedule(auto)
						for(int z=0; z < Nd; z++){
							gBestPosition[z] = bestParticleBuf[tempIndex*Nd+z];
						}
					}
				}
			}
			commTimer.startTimer();
			mpi::broadcast(world, &gBestPosition[0], Nd, 0); 
			commTimer.stopTimer();
			commTime += commTimer.getElapsedTime();
		}
		
		//------------------
		myTimer.stopTimer();
		fitnessTime += myTimer.getElapsedTime();
		velTimer.startTimer();
		//----------Update Velocities-------------
		w = wMax - ((wMax - wMin) / Nt) * j;
		
		#pragma omp parallel
		{
			#pragma omp for schedule(auto)
			for(int i = 0; i < localNp; i++){
				localSwarm[i]->updateVelocity(w, C1, C2, gBestPosition);
			}	
		}
		//------------------
		velTimer.stopTimer();
		velocityTime += velTimer.getElapsedTime();

	}
	


    totalTimer.stopTimer();
    totalTime = totalTimer.getElapsedTime();

    
	
	
	
	if(myRank == 0){
		std::cout << functionName    << ", "
			 << gBestValue      << ", "
			 << Np              << ", "
			 << Nd              << ", "
			 << positionTime    << ", "
			 << fitnessTime     << ", "
			 << velocityTime    << ", "
			 << commTime    	<< ", "
			 << totalTime       << ", "
			 << threads << ", "
			 << world.size() << "\n";		 
	}
}

void runPSO(double xMin, double xMax, double vMin, double vMax,
            double (*rPtr)(std::vector<double> &, int), std::string functionName)
{

    int Np, Nd, Nt;
    int NdMin, NdMax, NdStep;
    int NpMin, NpMax, NpStep;

    NdMin  = 100;  
    NdMax  = 100;
    NdStep = 10; // EXTRA CREDIT: Change NdMin to 10 and include results in evaluation for Extra Credit
    NpMin  = 100;  
    NpMax  = 1000;
    NpStep = 50;
    Nt = 300;  
    

    for (Np = NpMin; Np <= NpMax; Np += NpStep){
        for (Nd = NdMin; Nd <= NdMax; Nd += NdStep){
            for (int curTrial = 0; curTrial < 5; curTrial++){
                PSO(Np, Nd, Nt, xMin, xMax, vMin, vMax, rPtr, functionName);
            }
        }
    }

}
int main(){
	mpi::environment env;		
	mpi::communicator world;
    double (*rPtr)(std::vector<double> &, int) = NULL;
    double xMin, xMax, vMin, vMax;
	//std::ofstream performance;
	//	
	// if(world.rank() == 0){
		// performance.open( "results.csv" , std::ofstream::out | std::ofstream::trunc);
		// performance << "Function, Fitness, Np, Nd, Position Time, Fitness Time, Velocity Time, Comm Time, Total Time, Num Threads" << std::endl;
		// performance.close();
	// }

    rPtr = &Rastrigin;
    xMin = -5.12;
    xMax = 5.12;
    vMin = -1;
    vMax = 1;
	
	for (int i = 1; i < 11; i++) 
    {
		threads = i;
		runPSO(xMin, xMax, vMin, vMax, rPtr, "Hybrid");
	}

    rPtr = NULL;

    return 0;
}
